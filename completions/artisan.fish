function __artisan_using_no_command
	set cmd (commandline -opc)

	if [ (count $cmd) -eq 1 ]
		return 0
	end
	return 1
end

function __artisan_using_command
	set cmd (commandline -opc)

	if [ (count $cmd) -eq 2 ]
		if  contains $cmd[2] $argv
			return 0
		end
	end
	return 1
end

function __artisan_command_has_enough_parameters
  set cmd (commandline -opc)

  if [ (count $cmd) -ge (math $argv[1] + 2) ]; and contains $cmd[2] $argv[2..-1]
    return 0
  end
  return 1
end

function __artisan_complete_0
	complete -c artisan -f -n "__artisan_using_no_command" \
		-a "$argv[1]" \
		-d "$argv[2]"

	complete -c artisan -f -n "__artisan_command_has_enough_parameters 0 $argv[1]"
end


# clear-compiled
__artisan_complete_0 "clear-compiled" "Remove the compiled class file"


# down
__artisan_complete_0 "down" "Put the application into maintenance mode"


# env
__artisan_complete_0 "env" "Display the current framework environment"


# help
__artisan_complete_0 "help" "Displays help for a command"


# inspire
__artisan_complete_0 "inspire" "Display an inspiring quote"


# list
__artisan_complete_0 "list" "Lists commands"


# migrate
__artisan_complete_0 "migrate" "Run the database migrations"


# optimize
__artisan_complete_0 "optimize" "Cache the framework bootstrap files"


# preset
__artisan_complete_0 "preset" "Swap the front-end scaffolding for the application"


# serve
__artisan_complete_0 "serve" "Serve the application on the PHP development server"


# tinker
__artisan_complete_0 "tinker" "Interact with your application"


# up
__artisan_complete_0 "up" "Bring the application out of maintenance mode"


# auth:clear-resets
__artisan_complete_0 "auth:clear-resets" "Flush expired password reset tokens"


# cache:clear
__artisan_complete_0 "cache:clear" "Flush the application cache"


# cache:forget
__artisan_complete_0 "cache:forget" "Remove an item from the cache"


# cache:table
__artisan_complete_0 "cache:table" "Create a migration for the cache database table"





# config:cache
__artisan_complete_0 "config:cache" "Create a cache file for faster configuration loading"


# config:clear
__artisan_complete_0 "config:clear" "Remove the configuration cache file"


# db:seed
__artisan_complete_0 "db:seed" "Seed the database with records"


# db:wipe
__artisan_complete_0 "db:wipe" "Drop all tables, views, and types"


# event:cache
__artisan_complete_0 "event:cache" "Discover and cache the application's events and listeners"


# event:clear
__artisan_complete_0 "event:clear" "Clear all cached events and listeners"


# event:generate
__artisan_complete_0 "event:generate" "Generate the missing events and listeners based on registration"


# event:list
__artisan_complete_0 "event:list" "List the application's events and listeners"


# key:generate
__artisan_complete_0 "key:generate" "Set the application key"


# make:channel
__artisan_complete_0 "make:channel" "Create a new channel class"


# make:command
__artisan_complete_0 "make:command" "Create a new Artisan command"


# make:controller
__artisan_complete_0 "make:controller" "Create a new controller class"


# make:event
__artisan_complete_0 "make:event" "Create a new event class"


# make:exception
__artisan_complete_0 "make:exception" "Create a new custom exception class"


# make:factory
__artisan_complete_0 "make:factory" "Create a new model factory"


# make:job
__artisan_complete_0 "make:job" "Create a new job class"


# make:listener
__artisan_complete_0 "make:listener" "Create a new event listener class"


# make:mail
__artisan_complete_0 "make:mail" "Create a new email class"


# make:middleware
__artisan_complete_0 "make:middleware" "Create a new middleware class"


# make:migration
__artisan_complete_0 "make:migration" "Create a new migration file"


# make:model
__artisan_complete_0 "make:model" "Create a new Eloquent model class"


# make:notification
__artisan_complete_0 "make:notification" "Create a new notification class"


# make:observer
__artisan_complete_0 "make:observer" "Create a new observer class"


# make:policy
__artisan_complete_0 "make:policy" "Create a new policy class"


# make:provider
__artisan_complete_0 "make:provider" "Create a new service provider class"


# make:request
__artisan_complete_0 "make:request" "Create a new form request class"


# make:resource
__artisan_complete_0 "make:resource" "Create a new resource"


# make:rule
__artisan_complete_0 "make:rule" "Create a new validation rule"


# make:seeder
__artisan_complete_0 "make:seeder" "Create a new seeder class"


# make:test
__artisan_complete_0 "make:test" "Create a new test class"


# migrate:fresh
__artisan_complete_0 "migrate:fresh" "Drop all tables and re-run all migrations"


# migrate:install
__artisan_complete_0 "migrate:install" "Create the migration repository"


# migrate:refresh
__artisan_complete_0 "migrate:refresh" "Reset and re-run all migrations"


# migrate:reset
__artisan_complete_0 "migrate:reset" "Rollback all database migrations"


# migrate:rollback
__artisan_complete_0 "migrate:rollback" "Rollback the last database migration"


# migrate:rollback
__artisan_complete_0 "migrate:rollback" "Rollback the last database migration"


# migrate:status
__artisan_complete_0 "migrate:status" "Show the status of each migration"


# notifications:table
__artisan_complete_0 "notifications:table" "Create a migration for the notifications table"


# optimize:clear
__artisan_complete_0 "optimize:clear" "Remove the cached bootstrap files"


# package:discover
__artisan_complete_0 "package:discover" "Rebuild the cached package manifest"


# queue:failed
__artisan_complete_0 "queue:failed" "List all of the failed queue jobs"


# queue:failed-table
__artisan_complete_0 "queue:failed-table" "Create a migration for the failed queue jobs database table"


# queue:flush
__artisan_complete_0 "queue:flush" "Flush all of the failed queue jobs"


# queue:forget
__artisan_complete_0 "queue:forget" "Delete a failed queue job"


# queue:listen
__artisan_complete_0 "queue:listen" "Listen to a given queue"


# queue:restart
__artisan_complete_0 "queue:restart" "Restart queue worker daemons after their current job"


# queue:retry
__artisan_complete_0 "queue:retry" "Retry a failed queue job"


# queue:table
__artisan_complete_0 "queue:table" "Create a migration for the queue jobs database table"


# queue:work
__artisan_complete_0 "queue:work" "Start processing jobs on the queue as a daemon"


# route:cache
__artisan_complete_0 "route:cache" "Create a route cache file for faster route registration"


# route:clear
__artisan_complete_0 "route:clear" "Remove the route cache file"


# route:list
__artisan_complete_0 "route:list" "List all registered routes"


# schedule:run
__artisan_complete_0 "schedule:run" "Run the scheduled commands"


# session:table
__artisan_complete_0 "session:table" "Create a migration for the session database table"


# storage:link
__artisan_complete_0 "storage:link" "Create a symbolic link from "public/storage" to "storage/app/public""


# ui:auth
__artisan_complete_0 "ui:auth" "Scaffold basic login and registration views and routes"


# vendor:publish
__artisan_complete_0 "vendor:publish" "Publish any publishable assets from vendor packages"


# view:cache
__artisan_complete_0 "view:cache" "Compile all of the application's Blade templates"


# view:clear
__artisan_complete_0 "view:clear" "Clear all compiled view files"

